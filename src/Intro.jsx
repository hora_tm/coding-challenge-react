const interviewee = "Mohammad";
export default function Intro() {
  return (
    <div className="App p-2">
      <h1>Hello {interviewee}</h1>
      <p>
        Welcome to our coding challenge{" "}
        <span role="img" aria-label="Shaking Hand">
          👋
        </span>
      </p>
      <div className="text-start">
        <div>
          <span role="img" aria-label="Shaking Hand">
            🎨
          </span>{" "}
          First, let's take a look at {""}
          <a href="https://drive.google.com/drive/folders/1K1jILl06u9B8cQW8tSipsRhYtYhO3CnJ?usp=sharing">
            Designs!
          </a>
        </div>
        <div className="mt-2">
          The <strong>Goal</strong> is to implement that page using{" "}
          <span className="text-decoration-underline">React</span>, {""}
          <span className="text-decoration-underline">react-bootstrap</span>,
          and {""}
          <span className="text-decoration-underline">Axios</span>.
        </div>
        <div className="mt-4">
          <h5>
            <span role="img" aria-label="Shaking Hand">
              📜
            </span>
            Instructions:
          </h5>
          <ol>
            <li>
              You need to call our sample Api in order to dynamically render the
              page
              <div className="small text-secondary">
                API URL: https://y410v.mocklab.io/users/potato
              </div>
              <div className="small text-secondary">METHOD: GET</div>
            </li>
            <li>
              By calling that Api, you will receive a JSON with the following
              structure:
              <pre className="bg-light mt-2">
                {`
  {
    "section_list": [
        {
            "id": 0,
            "title": "Overview",
            "body": [
                {
                    "id": 0,
                    "label": "Full Name",
                    "value": "Mr. Potato"
                },
                ...
            ]
        },
        {
            "id": 1,
            "title": "Additional Info",
            "body": [
                {
                    "id": 0,
                    "name": "phone_number",
                    "label": "Phone Number"
                },
                ...
            ]
        },
        {
            "id": 2,
            "title": "Fields of Interest",
            "body": [
                {
                    "id": 0,
                    "category": "Side Dishes",
                    "items": [
                        {
                            "id": 0,
                            "label": "French Fries"
                        },
                        ...
                    ]
                },
                ...
            ]
        }
    ]
}
                `}
              </pre>
            </li>
            <li>Now, what are the tasks?</li>
          </ol>
        </div>
        <div className="mt-4">
          <h5>
            <span role="img" aria-label="Shaking Hand">
              ✅
            </span>{" "}
            Tasks:
          </h5>
          <ol>
            <li>You need to implement a custom hook for the Api call</li>
            <div className="small text-secondary">
              (we created the file for you in the root of the project tree:
              useFetch.js)
            </div>
            <li>
              Render the components based on the design and the Api response
            </li>
            <li>
              Finally, there is a <b>Save</b> button at the bottom of section 2.
              <br />
              Consider to bind a function which is named{" "}
              <i className="text-success">handleData</i> to that button in order
              to get all the values from the those inputs there.
            </li>
          </ol>
        </div>
        <div className="mt-4">
          <h5>
            <span role="img" aria-label="Shaking Hand">
              👮‍♂️
            </span>
            Rules:
          </h5>
          <ul>
            <li>
              No need to install anything. All the necessary packages are
              pre-installed!
            </li>
            <li>
              Only use the UI components provided by react-bootstrap {""}
              <a href="https://react-bootstrap.github.io/components/alerts/">
                (Docs)
              </a>
            </li>
            <div className="small text-secondary">
              (Suggestion: Accordion, Button, Col, Container, Form, Row)
            </div>
            <li>
              In the <b>Additional Info</b> JSON, some objects have a{" "}
              <i className="text-success">value</i> property and some of them
              don't. You may need to render those{" "}
              <i className="text-success">values</i> as a defaultValue for the
              inputs.
            </li>
            <li>
              Write your code (your component) inside the{" "}
              <i>{`Solution.jsx`}</i> file which is in the root of the project
              tree.
            </li>
            <li>You have 45 minutes to complete this task so mind the time.</li>
          </ul>
        </div>
        <h3>
          Good Luck {""}
          <span role="img" aria-label="Shaking Hand">
            ✌️
          </span>
        </h3>
      </div>
    </div>
  );
}
