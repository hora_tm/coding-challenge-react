import axios from "axios";
import { useState } from "react";
import { useCallback } from "react";

const useFetch = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const sendRequest = useCallback(async (requestConfig, applyData) => {
    setIsLoading(true);
    setError(null);

    await axios({
      method: requestConfig.method,
      url: requestConfig.url,
      headers: {
        Accept: "application/json",
      },
    })
      .then((res) => {
        applyData(res.data);
      })
      .catch((err) => {
        setError(err.message || "Something went wrong!");
      });

    setIsLoading(false);
  }, []);

  return {
    isLoading,
    error,
    sendRequest,
  };
};

export default useFetch;
