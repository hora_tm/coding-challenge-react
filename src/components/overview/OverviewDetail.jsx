import styles from "./OverviewDetail.module.css";
import { Form } from "react-bootstrap";
const OverviewDetail = (props) => {
  return (
    <>
      {
        <Form className="d-flex flex-wrap px-3">
          {props.data.map((item) => (
            <Form.Group
              key={item.id}
              className={
                styles.section_item +
                " col-4 my-2 d-flex flex-column align-items-start"
              }
            >
              <Form.Label>{item.label}</Form.Label>
              <Form.Control plaintext readOnly defaultValue={item.value} />
            </Form.Group>
          ))}
        </Form>
      }
    </>
  );
};

export default OverviewDetail;
