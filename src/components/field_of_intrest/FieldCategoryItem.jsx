import { Form } from "react-bootstrap";
import styles from "./FieldCategoryItem.module.css";

const FieldCategoryItem = ({ item }) => {
  return (
    <Form className="d-flex p-0">
      <Form.Check
        type="checkbox"
        label="Normal"
        className={styles.form_checkbox}
      />
      <Form.Check
        type="checkbox"
        label="Spicy"
        className={styles.form_checkbox}
      />
      <Form.Label>{item.label}</Form.Label>
    </Form>
  );
};

export default FieldCategoryItem;
