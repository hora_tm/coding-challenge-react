import FieldCategoryItem from "./FieldCategoryItem";
import styles from './FieldCategory.module.css'

const FieldItem = ({ field }) => {
    console.log(field);
  return (
    <div className="col-6 mb-3">
      <p className={styles.category_name} >{field.category}</p>
      {field.items.map((item) => (
        <FieldCategoryItem key={item.id} item={item} />
      ))}
    </div>
  );
};

export default FieldItem;
