import FieldCategory from "./FieldCategory";

const FieldsOfInterest = (props) => {
  return (
    <div className="d-flex flex-wrap px-4">
      {props.data.map((field) => (
        <FieldCategory key={field.id} field={field} />
      ))}
    </div>
  );
};

export default FieldsOfInterest;
