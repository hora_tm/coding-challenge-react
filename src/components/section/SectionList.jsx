import useFetch from "../../hooks/useFetch.js";
import React, { useEffect, useState } from "react";
import SectionItem from "./SectionItem";
import OverviewDetail from "../overview/OverviewDetail";
import FieldsOfInterest from "../field_of_intrest/FieldsOfInterest";
import AdditionalInfo from "../additional_info/AdditionalInfo";

const SectionList = () => {
  const { isLoading, error, sendRequest: fetchSections } = useFetch();
  const [activeSection, setActiveSection] = useState(null);
  const [sections, setSections] = useState([]);

  useEffect(() => {
    const setSectionList = (tasksObj) => {
      setSections(tasksObj.section_list);
    };

    fetchSections(
      {
        url: "https://my-json-server.typicode.com/ashkanahrabi/potato/db",
        method: "GET",
      },
      setSectionList
    );
  }, [fetchSections]);

  const setActiveSectionHandler = (activeSection) => {
    setActiveSection(activeSection);
  };

  return (
    <>
      {
        <div className="my-3 mx-4">
          {sections.map((section) => (
            <div key={section.id}>
              <SectionItem
                key={section.id}
                onSetActiveSection={setActiveSectionHandler}
                section={section}
                activeSection={activeSection}
              >
                {section.title == "Overview" && (
                  <OverviewDetail key="Overview" data={section.body} />
                )}
                {section.title == "Additional Info" && (
                  <AdditionalInfo key="AdditionalInfo" data={section.body} />
                )}
                {section.title == "Fields of Interest" && (
                  <FieldsOfInterest
                    key="FieldsOfInterest"
                    data={section.body}
                    activeSection={activeSection}
                  />
                )}
              </SectionItem>
            </div>
          ))}
        </div>
      }
    </>
  );
};

export default SectionList;
